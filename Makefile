PREFIX ?=	/usr/local
BINDIR ?=	bin
MANDIR ?=	man

all:

install:
	install -d ${DESTDIR}${PREFIX}${BINDIR} ${DESTDIR}${PREFIX}${MANDIR}/man1
	install -m 0755 totp_qr ${DESTDIR}${PREFIX}${BINDIR}
	install -m 0644 totp_qr.1 ${DESTDIR}${PREFIX}${MANDIR}/man1

.PHONY: all install
