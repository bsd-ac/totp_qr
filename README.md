## totp_qr

CLI utility to create a QR code for login_totp(8) that can be fed to any authenticator app.

For use with OpenBSD specifically but the code is simple enough to be useful to others.
